/**
 * Created by gigi on 8/31/14.
 */
$(function() {

    var options = {
        chart: {
            renderTo: "container"
        },
        title: {
            text: 'Under Markets - ' + $("#eventName").html(),
            x: -20 //center
        },
        subtitle: {
            text: 'by betfairBot',
            x: -20
        },
        xAxis: {
            title: {
                text: 'Minute'
            },
            categories: []
        },
        yAxis: {
            title: {
                text: 'Odds Value'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: []
    }

    // Create the chart
    var chart = new Highcharts.Chart(options);

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    $.get('getHighChartTableData?eventId=' + getParameterByName("eventId"), function(data) {
        // Split the lines
        var lines = data.split('\n');

        // Iterate over the lines and add categories or series
        $.each(lines, function(lineNo, line) {
            var items = line.split(',');

            // header line containes categories
            if (lineNo == 0) {
                $.each(items, function(itemNo, item) {
                    if (itemNo > 0) options.xAxis.categories.push(item);
                });
            }

            // the rest of the lines contain data with their name in the first
            // position
            else {
                var series = {
                    data: []
                };
                $.each(items, function(itemNo, item) {
                    if (itemNo == 0) {
                        series.name = item;
                    } else if (itemNo == 1) {
                        if (item === "false") {
                            series.visible = false;
                        } else {
                            series.visible = true;
                        }
                    } else {
                        series.data.push(parseFloat(item));
                    }
                });

                chart.addSeries(series);

            }

        });
    });






    /**
     * for each menu element, on mouseenter,
     * we enlarge the image, and show both sdt_active span and
     * sdt_wrap span. If the element has a sub menu (sdt_box),
     * then we slide it - if the element is the last one in the menu
     * we slide it to the left, otherwise to the right
     */
    $('#sdt_menu > li').bind('mouseenter',function(){
        var $elem = $(this);
        $elem.find('img')
            .stop(true)
            .animate({
                'width':'170px',
                'height':'170px',
                'left':'0px'
            },400,'easeOutBack')
            .andSelf()
            .find('.sdt_wrap')
            .stop(true)
            .animate({'top':'140px'},500,'easeOutBack')
            .andSelf()
            .find('.sdt_active')
            .stop(true)
            .animate({'height':'170px'},300,function(){
                var $sub_menu = $elem.find('.sdt_box');
                if($sub_menu.length){
                    var left = '170px';
                    if($elem.parent().children().length == $elem.index()+1)
                        left = '-170px';
                    $sub_menu.show().animate({'left':left},200);
                }
            });
    }).bind('mouseleave',function(){
        var $elem = $(this);
        var $sub_menu = $elem.find('.sdt_box');
        if($sub_menu.length)
            $sub_menu.hide().css('left','0px');

        $elem.find('.sdt_active')
            .stop(true)
            .animate({'height':'0px'},300)
            .andSelf().find('img')
            .stop(true)
            .animate({
                'width':'0px',
                'height':'0px',
                'left':'85px'},400)
            .andSelf()
            .find('.sdt_wrap')
            .stop(true)
            .animate({'top':'25px'},500);
    });

    if ($( "#datepicker" )) {
        try {
            $( "#datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
        } catch(e) {}
    }

    $("#os-phrases > h2").lettering('words').children("span").lettering().children("span").lettering();

});