<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/WEB-INF/pages/include.jsp" %>
<html>
<head>
    <title>Bet Partner Home</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Bet Partner" />
    <meta name="keywords" content="bet tip tips partner winnings"/>
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="../../resources/style/style.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="../../resources/style/lettering.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="../../resources/style/graph_chart.css" type="text/css" media="screen"/>



</head>


<body>

<div class="os-phrases" id="os-phrases">
    <h2></h2>
    <h2>Bet tips</h2>
    <h2>Robots</h2>
    <h2>Increase winnings</h2>
    <h2>Try our statistics</h2>
    <h2>Plan your strategy</h2>
    <h2>enjoy success</h2>
    <h2>learn different strategies</h2>
    <h2>share your ideea</h2>
</div>

<jsp:include page="menu.jsp"/>

<form action="addEventId" method="GET">
    <input type="text" name="eventId" placeholder="Please enter the event Id">
    <input type="submit" value="Add new Event">
</form>

<h1 id="eventName" style="color:#fff;">${eventStat.name}</h1>

<table class="zebra">
    <thead>
    <td></td>
    </thead>
    <tbody>
    <c:forEach var="entry" items="${eventIds}">
        <tr>
            <td style="color:#fff;">
                <a href="/chartv2?eventId=${entry.eventId}">${entry.eventId} - ${entry.name}</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>--%>
<script type="text/javascript" src="../../resources/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="../../resources/js/jquery.lettering.js"></script>
<script type="text/javascript" src="../../resources/js/main.js"></script>
<script type="text/javascript" src="../../resources/js/modernizr.custom.js"></script>
<script src="../../resources/js/highcharts.js"></script>
<script src="../../resources/js/exporting.js"></script>
</body>
</html>