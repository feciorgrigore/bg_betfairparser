<div class="content">
    <h1 class="title">Bet Partner</h1>
    <ul id="sdt_menu" class="sdt_menu">
        <li>
            <a href="/about">
                <img src="../../resources/images/2.jpg" alt=""/>
                <span class="sdt_active"></span>
						<span class="sdt_wrap">
							<span class="sdt_link">About Us</span>
							<span class="sdt_descr">What we offer</span>
						</span>
            </a>
        </li>
        <li>
            <a href="betting">
                <img src="../../resources/images/1.jpg" alt=""/>
                <span class="sdt_active"></span>
						<span class="sdt_wrap">
							<span class="sdt_link">Betting</span>
							<span class="sdt_descr">How to improve</span>
						</span>
            </a>
            <div class="sdt_box">
                <a href="#">How to bet</a>
                <a href="#">Where to bet</a>
                <a href="/strategies">Strategies</a>
            </div>
        </li>
        <li>
            <a href="/inspiration">
                <img src="../../resources/images/3.jpg" alt=""/>
                <span class="sdt_active"></span>
						<span class="sdt_wrap">
							<span class="sdt_link">Inspiration</span>
							<span class="sdt_descr">Where ideas get born</span>
						</span>
            </a>
        </li>
        <li>
            <a href="/statistics">
                <img src="../../resources/images/4.jpg" alt=""/>
                <span class="sdt_active"></span>
						<span class="sdt_wrap">
							<span class="sdt_link">Statistics</span>
							<span class="sdt_descr">Over 10k games</span>
						</span>
            </a>
            <div class="sdt_box">
                <a href="statsChart?interval=weekly&strategy=HT_X">View Graphs  Stats</a>
                <a href="/statistics">View Table Stats</a>
            </div>
        </li>
        <li>
            <a href="#">
                <img src="../../resources/images/5.jpg" alt=""/>
                <span class="sdt_active"></span>
						<span class="sdt_wrap">
							<span class="sdt_link">Blog</span>
							<span class="sdt_descr">I write about design</span>
						</span>
            </a>
        </li>
        <li>
            <a href="/bots">
                <img src="../../resources/images/6.jpg" alt=""/>
                <span class="sdt_active"></span>
						<span class="sdt_wrap">
							<span class="sdt_link">Bots</span>
							<span class="sdt_descr">I like to code</span>
						</span>
            </a>
            <div class="sdt_box">
                <a href="#">What is a bot</a>
                <a href="#">Results</a>
                <a href="#">Try a bot</a>
                <a href="#">Buy a license</a>
            </div>
        </li>
    </ul>
</div>