package com.mybot.delegate;

import com.betfair.aping.ApiNGDemo;
import com.betfair.aping.api.ApiNgOperations;
import com.betfair.aping.api.ApiNgRescriptOperations;
import com.betfair.aping.entities.*;
import com.betfair.aping.enums.*;
import com.betfair.aping.exceptions.APINGException;
import com.mybot.entity.EventStat;
import com.mybot.entity.MarketPrice;
import com.mybot.entity.MarketPriceEntry;
import com.mybot.entity.MarketStat;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by gigi on 9/15/14.
 */
@Component
public class BetfairDelegate {


    private ApiNgOperations rescriptOperations = ApiNgRescriptOperations.getInstance();
    private static final String O_0 = "Over/Under 0.5 Goals";
    private static final String O_1 = "Over/Under 1.5 Goals";
    private static final String O_2 = "Over/Under 2.5 Goals";
    private static final String O_3 = "Over/Under 3.5 Goals";
    private static final String O_4 = "Over/Under 4.5 Goals";
    private static final String O_5 = "Over/Under 5.5 Goals";
    private static final String O_6 = "Over/Under 6.5 Goals";
    private static final String O_7 = "Over/Under 7.5 Goals";
    private static final String O_8 = "Over/Under 8.5 Goals";
    private static final String O_9 = "Over/Under 9.5 Goals";


    public List<MarketStat> createMarketsForEvent(EventStat eventStat, String applicationKey, String sessionToken) {

        MarketFilter marketFilter;
        marketFilter = new MarketFilter();
        Set<String> eventTypeIds = new HashSet<String>();
        eventTypeIds.add("1");

        TimeRange time = new TimeRange();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -10);
        time.setFrom(calendar.getTime());
        Set<String> marketTypesCode = new HashSet<String>();
        marketFilter = new MarketFilter();
        marketFilter.setEventTypeIds(eventTypeIds);
        marketFilter.setMarketStartTime(time);
        marketFilter.setMarketTypeCodes(marketTypesCode);
        Set<String> eventIds = new HashSet<String>();
        eventIds.add(eventStat.getEventId());
        marketFilter.setEventIds(eventIds);
        Set<MarketProjection> marketProjection = new HashSet<MarketProjection>();
        marketProjection.add(MarketProjection.EVENT);
        String maxResults = "50";
        List<MarketCatalogue> marketCatalogueResult = null;

        try {
            marketCatalogueResult = rescriptOperations.listMarketCatalogue(marketFilter, marketProjection, MarketSort.FIRST_TO_START, maxResults,
                    applicationKey, sessionToken);
        } catch (APINGException e) {
            return null;
        }

        if (marketCatalogueResult.size() == 0) {
            return null;
        }
        List<MarketStat> result = new ArrayList<MarketStat>();
        for (MarketCatalogue market : marketCatalogueResult) {
            if (market.getMarketName().contains("Over/Under")) {
                result.add(processOverUnderMarket(market, eventStat));
            }
        }

        eventStat.setName(marketCatalogueResult.get(0).getEvent().getName());
        eventStat.setStartDate(marketCatalogueResult.get(0).getEvent().getOpenDate());

        return result;
    }

    private MarketStat processOverUnderMarket(MarketCatalogue marketCatalogue, EventStat eventStat) {
        MarketStat marketStat = new MarketStat();
        marketStat.setMarketName(marketCatalogue.getMarketName());
        marketStat.setMarketId(marketCatalogue.getMarketId());
        marketStat.setEventStat(eventStat);
        return marketStat;
    }


    public MarketPrice getMarketPrices(List<MarketStat> marketStats, EventStat eventStat, String applicationKey, String sessionToken) {


        List<String> marketIds = new ArrayList<String>();
        for (MarketStat marketStat : marketStats) {
            marketIds.add(marketStat.getMarketId());
        }

        PriceProjection priceProjection = new PriceProjection();
        Set<PriceData> priceData = new HashSet<PriceData>();
        priceData.add(PriceData.EX_BEST_OFFERS);
        priceProjection.setPriceData(priceData);

        List<MarketBook> marketBooks = null;
        try {
            marketBooks = rescriptOperations.listMarketBook(marketIds, priceProjection,
                    null, null, null, applicationKey, sessionToken);
        } catch (APINGException e) {
            return null;
        }

        if (marketBooks.size() == 0) {
            return null;
        }
        boolean isClosed = true;
        for (MarketBook marketBook : marketBooks) {
            if (marketBook.getStatus().equalsIgnoreCase("open")) {
                isClosed = false;
            }
        }
        if (isClosed) {
            throw new IllegalStateException("all markets are closed");
        }

        return processOverUnderPrice(marketBooks, eventStat, marketStats);
    }


    private MarketPrice processOverUnderPrice(List<MarketBook> marketBooks, EventStat eventStat, List<MarketStat> marketStats) {
        MarketPrice result = new MarketPrice();
        result.setTimestamp(new Date());
        result.setEventStat(eventStat);

        Map<String, MarketPriceEntry> marketPriceEntryMap = createMarketPriceEntryMap(marketBooks);
        for (MarketStat marketStat : marketStats) {
            if (marketStat.getMarketName().equals(O_0)) {
                result.setUnderOnePrice(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getPrice()));
                result.setUnderOneAmount(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getAmount()));
                result.setUnderOnePriceLay(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getLay()));
            }
            if (marketStat.getMarketName().equals(O_1)) {
                result.setUnderTwoPrice(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getPrice()));
                result.setUnderTwoAmount(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getAmount()));
                result.setUnderTwoPriceLay(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getLay()));
            }
            if (marketStat.getMarketName().equals(O_2)) {
                result.setUnderThreePrice(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getPrice()));
                result.setUnderThreeAmount(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getAmount()));
                result.setUnderThreePriceLay(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getLay()));
            }
            if (marketStat.getMarketName().equals(O_3)) {
                result.setUnderFourPrice(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getPrice()));
                result.setUnderFourAmount(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getAmount()));
                result.setUnderFourPriceLay(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getLay()));
            }
            if (marketStat.getMarketName().equals(O_4)) {
                result.setUnderFivePrice(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getPrice()));
                result.setUnderFiveAmount(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getAmount()));
                result.setUnderFivePriceLay(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getLay()));
            }
            if (marketStat.getMarketName().equals(O_5)) {
                result.setUnderSixPrice(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getPrice()));
                result.setUnderSixAmount(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getAmount()));
                result.setUnderSixPriceLay(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getLay()));
            }
            if (marketStat.getMarketName().equals(O_6)) {
                result.setUnderSevenPrice(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getPrice()));
                result.setUnderSevenAmount(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getAmount()));
                result.setUnderSevenPriceLay(String.valueOf(marketPriceEntryMap.get(marketStat.getMarketId()).getLay()));
            }
        }


        return result;
    }

    private Map<String, MarketPriceEntry>  createMarketPriceEntryMap(List<MarketBook> marketBooks) {
        Map<String, MarketPriceEntry> result = new HashMap<String, MarketPriceEntry>();
        for (MarketBook marketBook: marketBooks) {
            result.put(marketBook.getMarketId(), createMarketPriceEntry(marketBook));
        }
        return result;
    }

    private MarketPriceEntry createMarketPriceEntry(MarketBook marketBook) {
        MarketPriceEntry result =  new MarketPriceEntry();
        if (marketBook.getStatus().equals("OPEN")) {
            try {
                PriceSize correctPrizeSize = findCorrectPriceSize(marketBook.getRunners().get(0).getEx().getAvailableToBack());
                result.setAmount(correctPrizeSize.getSize());
                result.setPrice(correctPrizeSize.getPrice());
                result.setLay(marketBook.getRunners().get(0).getEx().getAvailableToLay().get(0).getPrice());
                result.setMarketId(marketBook.getMarketId());
            } catch (Exception e) {
                result.setAmount(-2d);
                result.setPrice(-2d);
                result.setLay(-2d);
                result.setMarketId(marketBook.getMarketId());
            }
        } else if (marketBook.getStatus().equals("SUSPENDED")) {
            result.setAmount(0d);
            result.setPrice(0d);
            result.setLay(0d);
            result.setMarketId(marketBook.getMarketId());
        } else if (marketBook.getStatus().equals("CLOSED")) {
            result.setAmount(-1d);
            result.setPrice(-1d);
            result.setLay(-1d);
            result.setMarketId(marketBook.getMarketId());
        } else {
            result.setAmount(-3d);
            result.setPrice(-3d);
            result.setMarketId(marketBook.getMarketId());
        }
        return result;
    }

    private PriceSize findCorrectPriceSize(List<PriceSize> priceSizes) {
        if (priceSizes.get(0).getPrice() >= priceSizes.get(1).getPrice()) {
            if (priceSizes.get(0).getPrice() >= priceSizes.get(2).getPrice()) {
                return priceSizes.get(0);
            } else {
                return priceSizes.get(2);
            }
        } else {
            if (priceSizes.get(1).getPrice() >= priceSizes.get(2).getPrice()) {
                return priceSizes.get(1);
            } else {
                return priceSizes.get(2);
            }
        }
    }
    
    public CurrentOrderSummaryReport getUnsettledBets(String applicationKey, String sessionToken) throws APINGException {

        TimeRange dateRange = new TimeRange();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -10);
        dateRange.setFrom(calendar.getTime());
        dateRange.setTo(new Date());

        CurrentOrderSummaryReport currentOrderSummaryReport =
                rescriptOperations.getUnmatchedBets(new HashSet<String>(), new HashSet<String>(), dateRange, OrderProjection.EXECUTABLE,
                        50, applicationKey, sessionToken);

        return  currentOrderSummaryReport;

    }



    public void getTomorrowsDesiredMarkets(String applicationKey, String sessionToken) throws APINGException {
        try {

            MarketFilter marketFilter;
            marketFilter = new MarketFilter();
            Set<String> eventTypeIds = new HashSet<String>();
            eventTypeIds.add("1");

            TimeRange time = new TimeRange();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            time.setFrom(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            time.setTo(calendar.getTime());


            Set<String> marketTypesCode = new HashSet<String>();
            marketTypesCode.add("MATCH_ODDS");


            marketFilter = new MarketFilter();
            marketFilter.setEventTypeIds(eventTypeIds);
            marketFilter.setMarketStartTime(time);
            marketFilter.setMarketTypeCodes(marketTypesCode);


            Set<MarketProjection> marketProjection = new HashSet<MarketProjection>();
            marketProjection.add(MarketProjection.EVENT);


            String maxResults = "50";

            List<MarketCatalogue> marketCatalogueResult = rescriptOperations.listMarketCatalogue(marketFilter, marketProjection, MarketSort.FIRST_TO_START, maxResults,
                    applicationKey, sessionToken);

            System.out.println("5. Print static marketId, name and runners....\n");
            printMarketCatalogue(marketCatalogueResult.get(0));

            PriceProjection priceProjection = new PriceProjection();
            Set<PriceData> priceData = new HashSet<PriceData>();
            priceData.add(PriceData.EX_BEST_OFFERS);
            priceProjection.setPriceData(priceData);

            List<String> marketIds = new ArrayList<String>();

            for (MarketCatalogue marketCatalogue : marketCatalogueResult) {
                marketIds.add(marketCatalogue.getMarketId());
            }
            
            

            List<MarketBook> marketBookReturn = rescriptOperations.listMarketBook(marketIds.subList(0, 10), priceProjection,
                    null, null, null, applicationKey, sessionToken);

            maxResults = "50";
        } catch (APINGException apiExc) {
            System.out.println(apiExc.toString());
        }
    }





    public void start(String applicationKey, String sessionToken) {

        try {

            MarketFilter marketFilter;
            marketFilter = new MarketFilter();
            Set<String> eventTypeIds = new HashSet<String>();
            eventTypeIds.add("1");

            /**
             * ListMarketCatalogue: Get next available horse races, parameters:
             * eventTypeIds : 7 - get all available horse races for event id 7 (horse racing)
             * maxResults: 1 - specify number of results returned (narrowed to 1 to get first race)
             * marketStartTime: specify date (must be in this format: yyyy-mm-ddTHH:MM:SSZ)
             * sort: FIRST_TO_START - specify sort order to first to start race
             */
            TimeRange time = new TimeRange();
            time.setFrom(new Date());

//            Set<String> countries = new HashSet<String>();
//            countries.add("GB");

            Set<String> marketTypesCode = new HashSet<String>();
            marketTypesCode.add("MATCH_ODDS");


            marketFilter = new MarketFilter();
            marketFilter.setEventTypeIds(eventTypeIds);
            marketFilter.setMarketStartTime(time);
//            marketFilter.setMarketCountries(countries);
            marketFilter.setMarketTypeCodes(marketTypesCode);


            Set<MarketProjection> marketProjection = new HashSet<MarketProjection>();
            marketProjection.add(MarketProjection.EVENT);
//            marketProjection.add(MarketProjection.MARKET_DESCRIPTION);
//            marketProjection.add(MarketProjection.MARKET_START_TIME);
//            marketProjection.add(MarketProjection.RUNNER_DESCRIPTION);


//            String maxResults = "50";
//
//            List<MarketCatalogue> marketCatalogueResult = rescriptOperations.listMarketCatalogue(marketFilter, marketProjection, MarketSort.FIRST_TO_START, maxResults,
//                    applicationKey, sessionToken);
//
//            System.out.println("5. Print static marketId, name and runners....\n");
//            printMarketCatalogue(marketCatalogueResult.get(0));
//            /**
//             * ListMarketBook: get list of runners in the market, parameters:
//             * marketId:  the market we want to list runners
//             *
//             */
//            System.out.println("6.(listMarketBook) Get volatile info for MarketStat including best 3 exchange prices available...\n");
//            String marketIdChosen = marketCatalogueResult.get(0).getMarketId();
//
            PriceProjection priceProjection = new PriceProjection();
            Set<PriceData> priceData = new HashSet<PriceData>();
            priceData.add(PriceData.EX_BEST_OFFERS);
            priceProjection.setPriceData(priceData);

            //In this case we don't need these objects so they are declared null
            OrderProjection orderProjection = null;
            MatchProjection matchProjection = MatchProjection.NO_ROLLUP;
            String currencyCode = null;

            List<String> marketIds = new ArrayList<String>();
            marketIds.add("1.115225332");

            List<MarketBook> marketBookReturn = rescriptOperations.listMarketBook(marketIds, priceProjection,
                    orderProjection, matchProjection, currencyCode, applicationKey, sessionToken);

            /**
             * PlaceOrders: we try to place a bet, based on the previous request we provide the following:
             * marketId: the market id
             * selectionId: the runner selection id we want to place the bet on
             * side: BACK - specify side, can be Back or Lay
             * orderType: LIMIT - specify order type
             * size: the size of the bet
             * price: the price of the bet
             * customerRef: 1 - unique reference for a transaction specified by user, must be different for each request
             *
             */

//            long selectionId = 0;
//            if ( marketBookReturn.size() != 0 ) {
//                Runner runner = marketBookReturn.get(0).getRunners().get(0);
//                selectionId = runner.getSelectionId();
//                System.out.println("7. Place a bet below minimum stake to prevent the bet actually " +
//                        "being placed for marketId: "+marketIdChosen+" with selectionId: "+selectionId+"...\n\n");
//                List<PlaceInstruction> instructions = new ArrayList<PlaceInstruction>();
//                PlaceInstruction instruction = new PlaceInstruction();
//                instruction.setHandicap(0);
//                instruction.setSide(Side.BACK);
//                instruction.setOrderType(OrderType.LIMIT);
//
//                LimitOrder limitOrder = new LimitOrder();
//                limitOrder.setPersistenceType(PersistenceType.LAPSE);
//                //API-NG will return an error with the default size=0.01. This is an expected behaviour.
//                //You can adjust the size and price value in the "apingdemo.properties" file
//                limitOrder.setPrice(getPrice());
//                limitOrder.setSize(getSize());
//
//                instruction.setLimitOrder(limitOrder);
//                instruction.setSelectionId(selectionId);
//                instructions.add(instruction);
//
//                String customerRef = "1";
//
//                PlaceExecutionReport placeBetResult = rescriptOperations.placeOrders(marketIdChosen, instructions, customerRef, applicationKey, sessionToken);
//
//                // Handling the operation result
//                if (placeBetResult.getStatus() == ExecutionReportStatus.SUCCESS) {
//                    System.out.println("Your bet has been placed!!");
//                    System.out.println(placeBetResult.getInstructionReports());
//                } else if (placeBetResult.getStatus() == ExecutionReportStatus.FAILURE) {
//                    System.out.println("Your bet has NOT been placed :*( ");
//                    System.out.println("The error is: " + placeBetResult.getErrorCode() + ": " + placeBetResult.getErrorCode().getMessage());
//                }
//            } else {
//                System.out.println("Sorry, no runners found\n\n");
//            }

        } catch (APINGException apiExc) {
            System.out.println(apiExc.toString());
        }
    }

    private static double getPrice() {

        try {
            return new Double((String) ApiNGDemo.getProp().get("BET_PRICE"));
        } catch (NumberFormatException e) {
            //returning the default value
            return new Double(1000);
        }

    }

    private static double getSize(){
        try{
            return new Double((String)ApiNGDemo.getProp().get("BET_SIZE"));
        } catch (NumberFormatException e){
            //returning the default value
            return new Double(0.01);
        }
    }

    private void printMarketCatalogue(MarketCatalogue mk){
        System.out.println("MarketStat Name: "+mk.getMarketName() + "; Id: "+mk.getMarketId()+"\n");
        List<RunnerCatalog> runners = mk.getRunners();
        if(runners!=null){
            for(RunnerCatalog rCat : runners){
                System.out.println("Runner Name: "+rCat.getRunnerName()+"; Selection Id: "+rCat.getSelectionId()+"\n");
            }
        }
    }

}
