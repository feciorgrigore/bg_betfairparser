package com.mybot.entity;

import com.betfair.aping.enums.Side;

import java.util.Date;

/**
 * Created by gigi on 9/27/2014.
 */
public class BetEntryJesus {

    private String eventName;
    private String marketName;
    private String marketId;
    private long selectionId;
    private double priceMatched;
    private Side side;
    private Date addedWhen;
    private Date cashOutWhen;
    private boolean cashedOut;
    private boolean cancelled;
    private String betfairBetId;


    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    public long getSelectionId() {
        return selectionId;
    }

    public void setSelectionId(long selectionId) {
        this.selectionId = selectionId;
    }

    public double getPriceMatched() {
        return priceMatched;
    }

    public void setPriceMatched(double priceMatched) {
        this.priceMatched = priceMatched;
    }

    public Side getSide() {
        return side;
    }

    public void setSide(Side side) {
        this.side = side;
    }

    public Date getAddedWhen() {
        return addedWhen;
    }

    public void setAddedWhen(Date addedWhen) {
        this.addedWhen = addedWhen;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getCashOutWhen() {
        return cashOutWhen;
    }

    public void setCashOutWhen(Date cashOutWhen) {
        this.cashOutWhen = cashOutWhen;
    }

    public boolean isCashedOut() {
        return cashedOut;
    }

    public void setCashedOut(boolean cashedOut) {
        this.cashedOut = cashedOut;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public String getBetfairBetId() {
        return betfairBetId;
    }

    public void setBetfairBetId(String betfairBetId) {
        this.betfairBetId = betfairBetId;
    }
}
