package com.mybot.entity;

import com.mobispine.genericdao.AbstractEntity;

import javax.persistence.*;

/**
 * Created by gigi on 9/15/14.
 */
@MappedSuperclass
public class AbstractBetfairBotEntity extends AbstractEntity<Long> {

    /**
     *
     */
    private static final long serialVersionUID = -750889793277962542L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ID")
    @Override
    public Long getId() {
        return id;
    }
}