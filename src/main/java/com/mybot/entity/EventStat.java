package com.mybot.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by gigi on 9/18/14.
 */
@Entity
@Table(name="eventStats")
public class EventStat extends AbstractBetfairBotEntity implements Serializable {

    private String name;
    private String eventId;
    private Date startDate;
    private String competition;
    private List<MarketStat> marketStats;
    private List<MarketPrice> marketPrices;
    private boolean closed;

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    @Temporal(value = TemporalType.TIMESTAMP)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getCompetition() {
        return competition;
    }

    public void setCompetition(String competition) {
        this.competition = competition;
    }

    @OneToMany(mappedBy = "eventStat", cascade = CascadeType.ALL)
    public List<MarketStat> getMarketStats() {
        return marketStats;
    }

    public void setMarketStats(List<MarketStat> marketStats) {
        this.marketStats = marketStats;
    }
    @OneToMany(mappedBy = "eventStat", cascade = CascadeType.ALL)
    public List<MarketPrice> getMarketPrices() {
        return marketPrices;
    }
    public void setMarketPrices(List<MarketPrice> marketPrices) {
        this.marketPrices = marketPrices;
    }


}