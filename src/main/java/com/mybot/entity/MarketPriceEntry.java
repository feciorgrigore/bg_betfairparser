package com.mybot.entity;

/**
 * Created by gigi on 9/18/14.
 */
public class MarketPriceEntry {

    private Double price;
    private Double amount;
    private Double lay;
    private String marketId;

    public Double getLay() {
        return lay;
    }

    public void setLay(Double lay) {
        this.lay = lay;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }
}
