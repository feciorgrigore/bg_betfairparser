package com.mybot.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by gigi on 9/15/14.
 */
@Entity
@Table(name="marketPrices")
public class MarketPrice extends AbstractBetfairBotEntity implements Serializable {

  private EventStat eventStat;
    private Date timestamp;
    private String underOnePrice = "0";
    private String underOneAmount = "0";
    private String underOnePriceLay = "0";
    private String underTwoPrice = "0";
    private String underTwoAmount = "0";
    private String underTwoPriceLay = "0";
    private String underThreePrice = "0";
    private String underThreeAmount = "0";
    private String underThreePriceLay = "0";
    private String underFourPrice = "0";
    private String underFourAmount = "0";
    private String underFourPriceLay = "0";
    private String underFivePrice = "0";
    private String underFiveAmount = "0";
    private String underFivePriceLay = "0";
    private String underSixPrice = "0";
    private String underSixAmount = "0";
    private String underSixPriceLay = "0";
    private String underSevenPrice = "0";
    private String underSevenAmount = "0";
    private String underSevenPriceLay = "0";


    public String getUnderOnePriceLay() {
        return underOnePriceLay;
    }

    public void setUnderOnePriceLay(String underOnePriceLay) {
        this.underOnePriceLay = underOnePriceLay;
    }

    public String getUnderTwoPriceLay() {
        return underTwoPriceLay;
    }

    public void setUnderTwoPriceLay(String underTwoPriceLay) {
        this.underTwoPriceLay = underTwoPriceLay;
    }

    public String getUnderThreePriceLay() {
        return underThreePriceLay;
    }

    public void setUnderThreePriceLay(String underThreePriceLay) {
        this.underThreePriceLay = underThreePriceLay;
    }

    public String getUnderFourPriceLay() {
        return underFourPriceLay;
    }

    public void setUnderFourPriceLay(String underFourPriceLay) {
        this.underFourPriceLay = underFourPriceLay;
    }

    public String getUnderFivePriceLay() {
        return underFivePriceLay;
    }

    public void setUnderFivePriceLay(String underFivePriceLay) {
        this.underFivePriceLay = underFivePriceLay;
    }

    public String getUnderSixPriceLay() {
        return underSixPriceLay;
    }

    public void setUnderSixPriceLay(String underSixPriceLay) {
        this.underSixPriceLay = underSixPriceLay;
    }

    public String getUnderSevenPriceLay() {
        return underSevenPriceLay;
    }

    public void setUnderSevenPriceLay(String underSevenPriceLay) {
        this.underSevenPriceLay = underSevenPriceLay;
    }

    public void setEventStat(EventStat eventStat) {
        this.eventStat = eventStat;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getUnderOnePrice() {
        return underOnePrice;
    }

    public void setUnderOnePrice(String underOnePrice) {
        this.underOnePrice = underOnePrice;
    }

    public String getUnderOneAmount() {
        return underOneAmount;
    }

    public void setUnderOneAmount(String underOneAmount) {
        this.underOneAmount = underOneAmount;
    }

    public String getUnderTwoPrice() {
        return underTwoPrice;
    }

    public void setUnderTwoPrice(String underTwoPrice) {
        this.underTwoPrice = underTwoPrice;
    }

    public String getUnderTwoAmount() {
        return underTwoAmount;
    }

    public void setUnderTwoAmount(String underTwoAmount) {
        this.underTwoAmount = underTwoAmount;
    }

    public String getUnderThreePrice() {
        return underThreePrice;
    }

    public void setUnderThreePrice(String underThreePrice) {
        this.underThreePrice = underThreePrice;
    }

    public String getUnderThreeAmount() {
        return underThreeAmount;
    }

    public void setUnderThreeAmount(String underThreeAmount) {
        this.underThreeAmount = underThreeAmount;
    }

    public String getUnderFourPrice() {
        return underFourPrice;
    }

    public void setUnderFourPrice(String underFourPrice) {
        this.underFourPrice = underFourPrice;
    }

    public String getUnderFourAmount() {
        return underFourAmount;
    }

    public void setUnderFourAmount(String underFourAmount) {
        this.underFourAmount = underFourAmount;
    }

    public String getUnderFivePrice() {
        return underFivePrice;
    }

    public void setUnderFivePrice(String underFivePrice) {
        this.underFivePrice = underFivePrice;
    }

    public String getUnderFiveAmount() {
        return underFiveAmount;
    }

    public void setUnderFiveAmount(String underFiveAmount) {
        this.underFiveAmount = underFiveAmount;
    }

    public String getUnderSixPrice() {
        return underSixPrice;
    }

    public void setUnderSixPrice(String underSixPrice) {
        this.underSixPrice = underSixPrice;
    }

    public String getUnderSixAmount() {
        return underSixAmount;
    }

    public void setUnderSixAmount(String underSixAmount) {
        this.underSixAmount = underSixAmount;
    }

    public String getUnderSevenPrice() {
        return underSevenPrice;
    }

    public void setUnderSevenPrice(String underSevenPrice) {
        this.underSevenPrice = underSevenPrice;
    }

    public String getUnderSevenAmount() {
        return underSevenAmount;
    }

    public void setUnderSevenAmount(String underSevenAmount) {
        this.underSevenAmount = underSevenAmount;
    }


    @ManyToOne
    @JoinColumn(name = "eventStat_id", insertable=true, updatable=true)
    public EventStat getEventStat() {
        return eventStat;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof MarketPrice) {
             if (!this.underOnePrice.equals(((MarketPrice) object).getUnderOnePrice())) {
                 return false;
             } else if (!this.underTwoPrice.equals(((MarketPrice) object).getUnderTwoPrice())) {
                 return false;
             } else if (!this.underThreePrice.equals(((MarketPrice) object).getUnderThreePrice())) {
                 return false;
             } else if (!this.underFourPrice.equals(((MarketPrice) object).getUnderFourPrice())) {
                 return false;
             } else if (!this.underFivePrice.equals(((MarketPrice) object).getUnderFivePrice())) {
                 return false;
             } else if (!this.underSixPrice.equals(((MarketPrice) object).getUnderSixPrice())) {
                 return false;
             } else if (!this.underSevenPrice.equals(((MarketPrice) object).getUnderSevenPrice())) {
                 return false;
             } else {
                return true;
             }
        } else {
            return false;
        }
    }
}
