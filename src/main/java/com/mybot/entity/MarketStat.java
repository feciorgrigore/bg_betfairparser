package com.mybot.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by gigi on 9/15/14.
 */
@Entity
@Table(name="marketStats")
public class MarketStat extends AbstractBetfairBotEntity implements Serializable {

    private String marketName;
    private String marketId;
    private EventStat eventStat;

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    @ManyToOne
    @JoinColumn(name = "eventStat_id", insertable=true, updatable=true)
    public EventStat getEventStat() {
        return eventStat;
    }

    public void setEventStat(EventStat eventStat) {
        this.eventStat = eventStat;
    }
}
