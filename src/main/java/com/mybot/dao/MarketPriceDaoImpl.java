package com.mybot.dao;

import com.mobispine.genericdao.Parameters;
import com.mobispine.genericdao.hibernate.AbstractHibernateDao;
import com.mybot.entity.EventStat;
import com.mybot.entity.MarketPrice;

import java.util.List;

/**
 * Created by gigi on 9/15/14.
 */
public class MarketPriceDaoImpl extends AbstractHibernateDao<MarketPrice, Long> implements MarketPriceDao {

    @Override
    public List<MarketPrice> loadMarketPricesForEvent(Long eventId) {
        Parameters parameters = new Parameters();
        parameters.equals("eventStat.id", eventId);
        List<MarketPrice> marketPrices = (List<MarketPrice>) this.loadByParameters(parameters);
        return marketPrices;
    }

    @Override
    public MarketPrice loadLatestMarketPricesForEvent(Long eventId) {
        List<MarketPrice> marketPrices =
                this.getHibernateTemplate().find("from MarketPrice mp where mp.id = (select max(m.id) from MarketPrice m where m.eventStat.id = ?)", eventId);

        if (marketPrices == null || marketPrices.size() == 0) {
            return null;
        } else {
            return marketPrices.get(0);
        }
    }
}
