package com.mybot.dao;

import com.mobispine.genericdao.DaoInterface;
import com.mybot.entity.EventStat;
import com.mybot.entity.MarketPrice;

import java.util.List;

/**
 * Created by gigi on 9/15/14.
 */
public interface MarketPriceDao extends DaoInterface<MarketPrice, Long> {

    public List<MarketPrice> loadMarketPricesForEvent(Long eventId);

    public MarketPrice loadLatestMarketPricesForEvent(Long eventId);
}
