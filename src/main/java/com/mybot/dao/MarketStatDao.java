package com.mybot.dao;

import com.mobispine.genericdao.DaoInterface;
import com.mybot.entity.EventStat;
import com.mybot.entity.MarketStat;

import java.util.List;

/**
 * Created by gigi on 9/15/14.
 */
public interface MarketStatDao extends DaoInterface<MarketStat, Long> {

    public List<MarketStat> loadByEventId(Long eventId);

    public void saveAll(List<MarketStat> marketStats);
}
