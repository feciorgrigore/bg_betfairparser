package com.mybot.dao;

import com.mobispine.genericdao.Parameters;
import com.mobispine.genericdao.hibernate.AbstractHibernateDao;
import com.mybot.entity.EventStat;
import com.mybot.entity.MarketStat;

import java.util.List;

/**
 * Created by gigi on 9/15/14.
 */
public class MarketStatDaoImpl extends AbstractHibernateDao<MarketStat, Long> implements MarketStatDao {

    @Override
    public List<MarketStat> loadByEventId(Long eventId) {
        Parameters parameters = new Parameters();
        parameters.equals("eventStat.id", eventId);
        List<MarketStat> result = this.loadByParameters(parameters);
        if (result == null || result.size() == 0) {
            return null;
        }
        return result;
    }

    @Override
    public void saveAll(List<MarketStat> marketStats) {
       for (MarketStat marketStat : marketStats) {
           this.saveOrUpdate(marketStat);
       }
    }
}
