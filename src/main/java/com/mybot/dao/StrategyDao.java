package com.mybot.dao;

import com.mobispine.genericdao.DaoInterface;
import com.mybot.campaign.strategy.Strategy;

/**
 * Created by gigi on 9/27/2014.
 */
public interface StrategyDao  extends DaoInterface<Strategy, Long> {
}
