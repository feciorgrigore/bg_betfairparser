package com.mybot.dao;

import com.mobispine.genericdao.hibernate.AbstractHibernateDao;
import com.mybot.campaign.TestCampaign;
import com.mybot.campaign.strategy.Strategy;

/**
 * Created by gigi on 9/27/2014.
 */
public class StrategyDaoImpl extends AbstractHibernateDao<Strategy, Long> implements StrategyDao {
}
