package com.mybot.dao;

import com.mobispine.genericdao.DaoInterface;
import com.mybot.campaign.BetEntity;

/**
 * Created by gigi on 9/27/2014.
 */
public interface BetEntityDao extends DaoInterface<BetEntity, Long> {
}
