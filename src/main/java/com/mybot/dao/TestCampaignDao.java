package com.mybot.dao;

import com.mobispine.genericdao.DaoInterface;
import com.mybot.campaign.TestCampaign;

/**
 * Created by gigi on 9/27/2014.
 */
public interface TestCampaignDao extends DaoInterface<TestCampaign, Long> {
}
