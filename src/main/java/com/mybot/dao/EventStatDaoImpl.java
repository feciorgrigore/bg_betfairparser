package com.mybot.dao;

import com.mobispine.genericdao.Parameter;
import com.mobispine.genericdao.Parameters;
import com.mobispine.genericdao.hibernate.AbstractHibernateDao;
import com.mybot.entity.EventStat;
import com.mybot.entity.MarketStat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by gigi on 9/15/14.
 */
public class EventStatDaoImpl extends AbstractHibernateDao<EventStat, Long> implements EventStatDao {

    @Override
    public void createOrUpdate(String eventId) {
        Parameters parameters = new Parameters();
        parameters.equals("eventId", eventId);
        EventStat eventStat = this.loadUniqueByParameters(parameters);
        if (eventStat == null) {
            eventStat = new EventStat();
            eventStat.setEventId(eventId);
            this.save(eventStat);
        }
    }

    @Override
    public List<EventStat> loadNewAddedEvents() {
        Parameters parameters = new Parameters();
        parameters.isNull("startDate");
        List<EventStat> result = this.loadByParameters(parameters);
        if (result == null) {
            return new ArrayList<EventStat>();
        }
        return result;
    }

    @Override
    public List<EventStat> loadInPlayEvents() {
        Parameters parameters = new Parameters();
        parameters.isNotNull("startDate");
//        parameters.greaterThan("startDate", new Date());
        parameters.equals("closed", false);
        List<EventStat> result = this.loadByParameters(parameters);
        if (result == null) {
            return new ArrayList<EventStat>();
        }
        return result;
    }

    @Override
    public EventStat loadByBetfairEventId(String eventId) {
        Parameters parameters = new Parameters();
        parameters.equals("eventId", eventId);
        EventStat eventStat = this.loadUniqueByParameters(parameters);
        return eventStat;
    }
}
