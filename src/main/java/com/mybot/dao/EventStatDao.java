package com.mybot.dao;

import com.mobispine.genericdao.DaoInterface;
import com.mybot.entity.EventStat;
import com.mybot.entity.MarketStat;

import java.util.List;

/**
 * Created by gigi on 9/15/14.
 */
public interface EventStatDao extends DaoInterface<EventStat, Long> {

    public void createOrUpdate(String eventId);

    public List<EventStat> loadNewAddedEvents();

    public List<EventStat> loadInPlayEvents();

    public EventStat loadByBetfairEventId(String eventId);
}
