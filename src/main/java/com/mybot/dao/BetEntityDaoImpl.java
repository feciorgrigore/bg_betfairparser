package com.mybot.dao;

import com.mobispine.genericdao.hibernate.AbstractHibernateDao;
import com.mybot.campaign.BetEntity;

/**
 * Created by gigi on 9/27/2014.
 */
public class BetEntityDaoImpl extends AbstractHibernateDao<BetEntity, Long> implements BetEntityDao {
}
