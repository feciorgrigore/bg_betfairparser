package com.mybot.controller;

import com.mobispine.genericdao.OrderCriteria;
import com.mobispine.genericdao.Parameter;
import com.mobispine.genericdao.Parameters;
import com.mybot.dao.EventStatDao;
import com.mybot.dao.MarketPriceDao;
import com.mybot.entity.EventStat;
import com.mybot.entity.MarketPrice;
import com.mybot.entity.MarketStat;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Controller
public class WebController {

    private static Logger logger = Logger.getLogger(WebController.class);

    @Autowired
    private EventStatDao eventStatDao;

    @Autowired
    private MarketPriceDao marketPriceDao;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String welcome(ModelMap model) {


        List<EventStat> eventStats = eventStatDao.loadAll();
        model.addAttribute("eventIds", eventStats);

      return "index";
	}

    @RequestMapping(value = "/addEventId", method = RequestMethod.GET)
    public String addEventId(ModelMap model, HttpServletRequest request) {

        String eventId = request.getParameter("eventId");
        if (eventId != null) {
            eventStatDao.createOrUpdate(eventId);
            List<EventStat> eventStats = eventStatDao.loadAll();
            model.addAttribute("eventIds", eventStats);
        } else {
            model.addAttribute("errorMessage", "The eventId is empty");
        }

        return "chartv2";
    }

    @RequestMapping(value = "/chart", method = RequestMethod.GET)
    public String chart(ModelMap model, HttpServletRequest request) {
        model.addAttribute("eventId", request.getParameter("eventId"));
        model.addAttribute("underX", request.getParameter("underX"));

        String eventIdString = request.getParameter("eventId");
        Parameters parameters = new Parameters();
        parameters.equals("eventId", eventIdString);
        EventStat eventStat = eventStatDao.loadUniqueByParameters(parameters);
        model.addAttribute("eventStat", eventStat);

        List<EventStat> eventStats = eventStatDao.loadAll();
        model.addAttribute("eventIds", eventStats);


        return "chart";
    }

    @RequestMapping(value = "/chartSettings", method = RequestMethod.GET)
    public String chartSettings(ModelMap model, HttpServletRequest request) {
       return "chartSettings";
    }


    @RequestMapping(value = "/charData", method = RequestMethod.GET)
    public String charData(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        StringBuffer data = new StringBuffer();
        String eventIdString = request.getParameter("eventId");
        String underX = request.getParameter("underX");
        Parameters parameters = new Parameters();
        parameters.equals("eventId", eventIdString);
        EventStat eventStat = eventStatDao.loadUniqueByParameters(parameters);

        List<MarketPrice> marketPrices = marketPriceDao.loadMarketPricesForEvent(eventStat.getId());

        for (int i=0; i< marketPrices.size(); i++) {
            Date startDate = eventStat.getStartDate();
            long duration = marketPrices.get(i).getTimestamp().getTime() - startDate.getTime();
            Long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
            if (underX.equals("1")) {
                data.append(String.valueOf(diffInMinutes.intValue()) + ";"
                        + marketPrices.get(i).getUnderTwoPrice() + ";"
                        + marketPrices.get(i).getUnderThreePrice() + ";"
                        + "\n");
            } else if (underX.equals("2")) {
                data.append(String.valueOf(diffInMinutes.intValue()) + ";"
                        + marketPrices.get(i).getUnderThreePrice() + ";"
                        + marketPrices.get(i).getUnderFourPrice() + ";"
                        + "\n");
            } else if (underX.equals("3")) {
                data.append(String.valueOf(diffInMinutes.intValue()) + ";"
                        + marketPrices.get(i).getUnderFourPrice() + ";"
                        + marketPrices.get(i).getUnderFivePrice() + ";"
                        + "\n");
            } else if (underX.equals("4")) {
                data.append(String.valueOf(diffInMinutes.intValue()) + ";"
                        + marketPrices.get(i).getUnderFivePrice() + ";"
                        + marketPrices.get(i).getUnderSixPrice() + ";"
                        + "\n");
            } else if (underX.equals("5")) {
                data.append(String.valueOf(diffInMinutes.intValue()) + ";"
                        + marketPrices.get(i).getUnderSixPrice() + ";"
                        + marketPrices.get(i).getUnderSevenPrice() + ";"
                        + "\n");
            } else {
                data.append(String.valueOf(diffInMinutes.intValue()) + ";"
                        + marketPrices.get(i).getUnderOnePrice() + ";"
                        + marketPrices.get(i).getUnderTwoPrice() + ";"
                        + marketPrices.get(i).getUnderThreePrice() + ";"
                        + marketPrices.get(i).getUnderFourPrice() + ";"
                        + marketPrices.get(i).getUnderFivePrice() + ";"
                        + marketPrices.get(i).getUnderSixPrice() + ";"
                        + marketPrices.get(i).getUnderSevenPrice() +
                        "\n");
            }


        }

        response.getWriter().append(data.toString());
        return null;
    }


    @RequestMapping(value = "/chartv2", method = RequestMethod.GET)
    public String chartv2(ModelMap model, HttpServletRequest request) {
        String eventIdString = request.getParameter("eventId");
        Parameters parameters = new Parameters();
        parameters.equals("eventId", eventIdString);
        EventStat eventStat = eventStatDao.loadUniqueByParameters(parameters);

        List<MarketPrice> marketPrices = marketPriceDao.loadMarketPricesForEvent(eventStat.getId());

        Parameters empty = new Parameters();
        OrderCriteria orderCriteria = new OrderCriteria("startDate", true);

        List<EventStat> eventStats = eventStatDao.loadByParameters(empty, orderCriteria);
        model.addAttribute("eventIds", eventStats);
        model.addAttribute("eventStat", eventStat);

        return "chartv2";
    }

    @RequestMapping(value = "/getHighChartTableData", method = RequestMethod.GET)
    public String getHighChartTableData(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        StringBuffer data = new StringBuffer();
        String eventIdString = request.getParameter("eventId");
        Parameters parameters = new Parameters();
        parameters.equals("eventId", eventIdString);
        EventStat eventStat = eventStatDao.loadUniqueByParameters(parameters);
        List<MarketPrice> marketPrices = marketPriceDao.loadMarketPricesForEvent(eventStat.getId());


        data.append("0");
        for (int i=0; i< marketPrices.size(); i++) {
            long minute = (marketPrices.get(i).getTimestamp().getTime()/60000) - (eventStat.getStartDate().getTime()/60000);
            data.append("," + (int) minute);
        }
        data.append("\n");

        data.append("Under 0.5,true");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderOnePrice() == null ? "1.0" : marketPrices.get(i).getUnderOnePrice()));
        }
        data.append("\n");

        data.append("Under 1.5,true");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderTwoPrice() == null ? "1.0" : marketPrices.get(i).getUnderTwoPrice()));
        }
        data.append("\n");

        data.append("Under 2.5,true");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderThreePrice() == null ? "1.0" : marketPrices.get(i).getUnderThreePrice()));
        }
        data.append("\n");

        data.append("Under 3.5,true");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderFourPrice() == null ? "1.0" : marketPrices.get(i).getUnderFourPrice()));
        }
        data.append("\n");

        data.append("Under 4.5,true");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderFivePrice() == null ? "1.0" : marketPrices.get(i).getUnderFivePrice()));
        }
        data.append("\n");

        data.append("Under 5.5,true");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderSixPrice() == null ? "1.0" : marketPrices.get(i).getUnderSixPrice()));
        }
        data.append("\n");

        data.append("Under 6.5,true");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderSevenPrice() == null ? "1.0" : marketPrices.get(i).getUnderSevenPrice()));
        }
        data.append("\n");


        // lay price data
        data.append("Lay Under 0.5,false");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderOnePriceLay() == null ? "1.0" : marketPrices.get(i).getUnderOnePriceLay()));
        }
        data.append("\n");

        data.append("Lay Under 1.5,false");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderTwoPriceLay() == null ? "1.0" : marketPrices.get(i).getUnderTwoPriceLay()));
        }
        data.append("\n");

        data.append("Lay Under 2.5,false");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderThreePriceLay() == null ? "1.0" : marketPrices.get(i).getUnderThreePriceLay()));
        }
        data.append("\n");

        data.append("Lay Under 3.5,false");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderFourPriceLay() == null ? "1.0" : marketPrices.get(i).getUnderFourPriceLay()));
        }
        data.append("\n");

        data.append("Lay Under 4.5,false");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderFivePriceLay() == null ? "1.0" : marketPrices.get(i).getUnderFivePriceLay()));
        }
        data.append("\n");

        data.append("Lay Under 5.5,false");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderSixPriceLay() == null ? "1.0" : marketPrices.get(i).getUnderSixPriceLay()));
        }
        data.append("\n");


        data.append("Lay Under 6.5,false");
        for (int i=0; i< marketPrices.size(); i++) {
            data.append(", " + (marketPrices.get(i).getUnderSevenPriceLay() == null ? "1.0" : marketPrices.get(i).getUnderSevenPriceLay()));
        }


        response.getWriter().append(data.toString());
        return null;
    }


}