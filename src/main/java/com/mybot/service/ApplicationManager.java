package com.mybot.service;

import com.mybot.dao.EventStatDao;
import com.mybot.dao.MarketPriceDao;
import com.mybot.dao.MarketStatDao;
import com.mybot.delegate.BetfairDelegate;
import com.mybot.entity.EventStat;
import com.mybot.entity.MarketPrice;
import com.mybot.entity.MarketStat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Created by gigi on 9/15/14.
 */
@Component
public class ApplicationManager {

    private static Logger logger = Logger.getLogger(ApplicationManager.class);

    @Autowired
    private BetfairDelegate betfairDelegate;

    @Autowired
    private EventStatDao eventStatDao;

    @Autowired
    private MarketStatDao marketStatDao;

    @Autowired
    private MarketPriceDao marketPriceDao;

    // obtained from https://developer.betfair.com/visualisers/api-ng-account-operations/
//    String applicationKey = "uSmjWwT0FCxRoXCI";
    String applicationKey = "6pe58dqB7pJKoQwV";

    String sessionToken = "4q8PCjxJc0pY6xxppL27H9rq5cx57eXMni92M+Hspj8=";


    @Scheduled(cron="*/15 * * * * ?")
    public void getNewAddedEvents() throws IOException {

        List<EventStat> eventStats = eventStatDao.loadNewAddedEvents();
        if (eventStats.size() == 0) {
            logger.info("No new events added.");
        }
        for (EventStat event : eventStats) {
            List<MarketStat> markets = marketStatDao.loadByEventId(event.getId());
            if (markets == null) {
                logger.info("Will try to create markets for event: " + event.getEventId() + " [eventId=" + event.getId() + "]");
                markets = betfairDelegate.createMarketsForEvent(event, applicationKey, sessionToken);
                if (markets == null || markets.size() == 0) {
                    // something went wrong - drop and repeat next time
                    logger.info("Failure trying to get markets for event: "+ event.getEventId() + " [eventId=" + event.getId() + "] - will retry later");
                    continue;
                } else {
                    marketStatDao.saveAll(markets);
                    eventStatDao.saveOrUpdate(event);
                    logger.info("Success: " + markets.size() + " markets created for " + event.getEventId() + " [eventId=" + event.getId() + "]");
                }
            }
        }
    }

    @Scheduled(cron="*/1 * * * * ?")
    public void getStatsPricesForInPlayEvents() throws IOException {
        List<EventStat> eventStats = eventStatDao.loadInPlayEvents();
        if (eventStats.size() == 0) {
            logger.info("No in play events.");
        }
        for (EventStat event : eventStats) {
            logger.info("Will try to get prices for event: " + event.getEventId() + " [eventId=" + event.getId() + "]");
            List<MarketStat> markets = marketStatDao.loadByEventId(event.getId());
            if (markets == null) {
                logger.info("Something is wrong - No markets for event: " + event.getEventId() + " [eventId=" + event.getId() + "]");
                continue;
            } else {
                MarketPrice marketPrice = null;
                try {
                    marketPrice = betfairDelegate.getMarketPrices(markets,event, applicationKey, sessionToken);
                    if (marketPrice == null) {
                        logger.info("Failure while trying to get prices for event: " + event.getEventId() + " [eventId=" + event.getId() + "]");
                    } else {
                        // see if odds are different than the previous ones
                        MarketPrice lastMarketPrice = marketPriceDao.loadLatestMarketPricesForEvent(event.getId());
                        if (lastMarketPrice == null || !lastMarketPrice.equals(marketPrice)) {
                            marketPriceDao.save(marketPrice);
                            logger.info("New market prices: " + marketPrice.getId() + " for event: " + event.getEventId() + " [eventId=" + event.getId() + "]");
                        } else {
                            logger.info("Unmodified market prices for event: " + event.getEventId() + " [eventId=" + event.getId() + "]");
                        }
                    }
                } catch (IllegalStateException e) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(event.getStartDate());
                    calendar.add(Calendar.HOUR_OF_DAY, 2);
                    if (calendar.getTime().before(new Date())) {
                        event.setClosed(true);
                        eventStatDao.saveOrUpdate(event);
                    }
                }
            }
        }
    }
}
