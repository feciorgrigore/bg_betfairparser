package com.mybot.service;

import com.mybot.dao.EventStatDao;
import com.mybot.dao.MarketPriceDao;
import com.mybot.dao.MarketStatDao;
import com.mybot.delegate.BetfairDelegate;
import com.mybot.delegate.BetfairDelegateSecondary;
import com.mybot.entity.EventStat;
import com.mybot.entity.MarketPrice;
import com.mybot.entity.MarketStat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Created by gigi on 9/15/14.
 */
@Component
public class ApplicationManagerSecondary {

    private static Logger logger = Logger.getLogger(ApplicationManager.class);

    @Autowired
    private BetfairDelegateSecondary betfairDelegate;

    @Autowired
    private EventStatDao eventStatDao;

    @Autowired
    private MarketStatDao marketStatDao;

    @Autowired
    private MarketPriceDao marketPriceDao;

    // obtained from https://developer.betfair.com/visualisers/api-ng-account-operations/
    String applicationKey = "wx0IG498b0lybPWr";
    String sessionToken = "dp5D+PbWpQ8IOWeqPZxaKTLn27QsPSAK1Ni8pqlpkRA=";


    //@Scheduled(cron="*/30 * * * * ?")
    public void getNewAddedEvents() throws IOException {

        List<EventStat> eventStats = eventStatDao.loadNewAddedEvents();
        if (eventStats.size() == 0) {
            logger.info("No new events added.");
        }
        for (EventStat event : eventStats) {
            List<MarketStat> markets = marketStatDao.loadByEventId(event.getId());
            if (markets == null) {
                logger.info("Will try to create markets for event: " + event.getEventId() + " [eventId=" + event.getId() + "]");
                markets = betfairDelegate.createMarketsForEvent(event, applicationKey, sessionToken);
                if (markets == null || markets.size() == 0) {
                    // something went wrong - drop and repeat next time
                    logger.info("Failure trying to get markets for event: "+ event.getEventId() + " [eventId=" + event.getId() + "] - will retry later");
                    continue;
                } else {
                    marketStatDao.saveAll(markets);
                    eventStatDao.saveOrUpdate(event);
                    logger.info("Success: " + markets.size() + " markets created for " + event.getEventId() + " [eventId=" + event.getId() + "]");
                }
            }
        }
    }

    //@Scheduled(cron="*/1 * * * * ?")
    public void getStatsPricesForInPlayEvents() throws IOException {
        List<EventStat> eventStats = eventStatDao.loadInPlayEvents();
        if (eventStats.size() == 0) {
            logger.info("No in play events.");
        }
        Calendar calendar = Calendar.getInstance();
        for (EventStat event : eventStats) {
            logger.info("Will try to get prices for event: " + event.getEventId() + " [eventId=" + event.getId() + "]");
            calendar.setTime(event.getStartDate());
            calendar.add(Calendar.HOUR_OF_DAY, 2);
            if (event.getStartDate().after(calendar.getTime())) {
                event.setClosed(true);
                eventStatDao.saveOrUpdate(event);
            }
            List<MarketStat> markets = marketStatDao.loadByEventId(event.getId());
            if (markets == null) {
                logger.info("Something is wrong - No markets for event: " + event.getEventId() + " [eventId=" + event.getId() + "]");
                continue;
            } else {
                boolean closed = false;

                MarketPrice marketPrice = null;
                try {
                    marketPrice = betfairDelegate.getMarketPrices(markets,event, applicationKey, sessionToken, closed);
                    if (marketPrice == null) {
                        logger.info("Failure while trying to get prices for event: " + event.getEventId() + " [eventId=" + event.getId() + "]");
                    } else {
                        marketPriceDao.save(marketPrice);
                    }
                } catch (IllegalStateException e) {
                    event.setClosed(true);
                    eventStatDao.saveOrUpdate(event);
                }
            }
        }
    }

   // @Scheduled(cron="*/1 * * * * ?")
    public void followEventToPlaceBet() throws IOException {
        List<EventStat> eventStats = eventStatDao.loadInPlayEvents();
        if (eventStats.size() == 0) {
            logger.info("No followed events to bet on.");
        }
        Calendar calendar = Calendar.getInstance();
        for (EventStat event : eventStats) {
            logger.info("Following event: " + event.getEventId() + " [eventId=" + event.getId() + "]");
            calendar.setTime(event.getStartDate());
            calendar.add(Calendar.HOUR_OF_DAY, 2);
            if (event.getStartDate().after(calendar.getTime())) {
                event.setClosed(true);
                eventStatDao.saveOrUpdate(event);
            }
            List<MarketStat> markets = marketStatDao.loadByEventId(event.getId());
            if (markets == null) {
                logger.info("Something is wrong - No markets for event: " + event.getEventId() + " [eventId=" + event.getId() + "]");
                continue;
            } else {
                boolean closed = false;
                boolean betPlaced = false;
                try {
                    // follow an event and place bet when an event occurred
                    betPlaced = betfairDelegate.followEventForPlacingBet(markets,event, applicationKey, sessionToken, closed);
                    if (betPlaced) {
                        // TODO: if a bet was placed keep track of prices and do cash out when profit is achieved or after a established period of time
                        // will do cash out even if there will be a small loss
                        logger.info("Placed bet on event : " + event.getEventId() + " [eventId=" + event.getId() + "].");
                    } else {
                        logger.info("Failed placing bet on event : " + event.getEventId() + " [eventId=" + event.getId() + "].");
                    }
                } catch (IllegalStateException e) {
                    event.setClosed(true);
                    eventStatDao.saveOrUpdate(event);
                }
            }
        }
    }
}
