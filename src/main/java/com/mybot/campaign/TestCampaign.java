package com.mybot.campaign;

import com.mybot.campaign.strategy.Strategy;
import com.mybot.campaign.strategy.Under3p5BackStrategy;
import com.mybot.entity.AbstractBetfairBotEntity;
import com.mybot.entity.EventStat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by gigi on 9/27/2014.
 */
@Entity
@Table(name="testCampaigns")
public class TestCampaign extends AbstractBetfairBotEntity implements Serializable {

    private Strategy strategy = new Under3p5BackStrategy();
    private Date timstamp;
    private boolean finished;

    @OneToOne(fetch=FetchType.EAGER, cascade = CascadeType.ALL)
    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public Date getTimstamp() {
        return timstamp;
    }

    public void setTimstamp(Date timstamp) {
        this.timstamp = timstamp;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

}
