package com.mybot.campaign;

import com.mybot.campaign.strategy.Strategy;
import com.mybot.entity.MarketPrice;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Created by gigi on 9/27/2014.
 */
@Component
public class TestExecutionFlow {
    private static Logger logger = Logger.getLogger(TestExecutionFlow.class);


    /**
     * starting point
     * @param marketPrice
     */
    public void feedMarketPrice(MarketPrice marketPrice, Strategy strategy) {

        if (verifyInvalidData(marketPrice)) {
            verifyDataAgainstStrategy(marketPrice, strategy);
        } else {
            logger.info("invalid data for market price wit Id " + marketPrice.getId());
        }
    }


    private boolean verifyInvalidData(MarketPrice marketPrice) {
        return true;
    }

    private void verifyDataAgainstStrategy(MarketPrice marketPrice, Strategy strategy) {
        BetEntity strategyResult = strategy.applyStrategy(marketPrice);
        if (strategyResult != null) {
            placeBet(strategyResult);
        }
    }



    private void placeBet(BetEntity betEntity) {
        logger.info("will place bet on " + betEntity.toString());
    }

}
