package com.mybot.campaign.strategy;

import com.mybot.campaign.BetEntity;
import com.mybot.entity.MarketPrice;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by gigi on 9/27/2014.
 */
@Entity
@Table(name="under3p5")
public class Under3p5BackStrategy extends  Strategy {
    @Override
    public BetEntity applyStrategy(MarketPrice marketPrice) {
        BetEntity betEntity = new BetEntity();
        betEntity.setSelectionId(1l);
        betEntity.setMarketId("123452342");

        // will return null is strategy's logic is not a match, a betEntry otherwise
        return betEntity;
    }
}
