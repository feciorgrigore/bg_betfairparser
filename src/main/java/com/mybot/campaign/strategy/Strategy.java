package com.mybot.campaign.strategy;

import com.mybot.campaign.BetEntity;
import com.mybot.entity.AbstractBetfairBotEntity;
import com.mybot.entity.MarketPrice;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by gigi on 9/27/2014.
 */
@Entity
@Inheritance(strategy= InheritanceType.JOINED)
@Table(name="strategies")
public abstract class Strategy extends AbstractBetfairBotEntity implements Serializable {

    public abstract BetEntity applyStrategy(MarketPrice marketPrice);
}
