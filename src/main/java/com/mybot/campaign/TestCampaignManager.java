package com.mybot.campaign;

import com.mybot.campaign.strategy.Under3p5BackStrategy;
import com.mybot.dao.EventStatDao;
import com.mybot.dao.MarketPriceDao;
import com.mybot.dao.TestCampaignDao;
import com.mybot.entity.EventStat;
import com.mybot.entity.MarketPrice;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by gigi on 9/27/2014.
 */
@Component
public class TestCampaignManager {
    private static Logger logger = Logger.getLogger(TestCampaignManager.class);

    @Autowired
    private EventStatDao eventStatDao;

    @Autowired
    private TestExecutionFlow testExecutionFlow;

    @Autowired
    private MarketPriceDao marketPriceDao;

    @Autowired
    private TestCampaignDao testCampaignDao;

    private List<String> betfairEventIds = Arrays.asList("27271819", "27270742");

    @Scheduled(cron="*/5 * * * * ?")
    public void startCampaign() {
        TestCampaign testCampaign = new TestCampaign();
        testCampaign.setFinished(false);
        testCampaign.setStrategy(new Under3p5BackStrategy());


        Map<EventStat, List<MarketPrice>> eventStatPrices = null;

        if (!testCampaign.isFinished()) {
            eventStatPrices = this.loadMarketPricesByEventStatIds(betfairEventIds);
            for (Map.Entry<EventStat, List<MarketPrice>> entry : eventStatPrices.entrySet()) {
                logger.info("Will start feed market prices for event: " + entry.getKey().getId() + " (betfairEventId= " + entry.getKey().getEventId() + ")");
                for (MarketPrice marketPrice : entry.getValue()) {
                    logger.info("Will feed marketPrice with Id= " + marketPrice.getId());
                    testExecutionFlow.feedMarketPrice(marketPrice, testCampaign.getStrategy());
                }
            }
        }

    }

    public Map<EventStat, List<MarketPrice>> loadMarketPricesByEventStatIds(List<String> eventStatIds) {
        Map<EventStat, List<MarketPrice>> result = new HashMap<EventStat, List<MarketPrice>>();

        for (String eventStatId : eventStatIds) {
            EventStat eventStat = eventStatDao.loadByBetfairEventId(eventStatId);
            if (eventStat != null) {
                List<MarketPrice> marketPrices = marketPriceDao.loadMarketPricesForEvent(eventStat.getId());
                if (marketPrices != null && marketPrices.size() > 0) {
                    result.put(eventStat, marketPrices);
                }
            }
        }
        return result;
    }
}
