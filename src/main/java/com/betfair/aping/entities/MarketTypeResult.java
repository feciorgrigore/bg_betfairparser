package com.betfair.aping.entities;


import javax.annotation.Generated;

@Generated("pt.paulosantos.betfair.aping.codegen")
public class MarketTypeResult {
    /**
    * MarketStat Type
    */
    private String marketType;
    /**
    * Count of markets associated with this marketType
    */
    private Integer marketCount;


    //#######################
    //# GETTERS AND SETTERS #
    //#######################
     
    /**
    * MarketStat Type
    */

    public String getMarketType(){
        return marketType;
    }
     
    /**
    * MarketStat Type
    */

    public void setMarketType(String marketType){
        this.marketType = marketType;
    }
     
     
    /**
    * Count of markets associated with this marketType
    */

    public Integer getMarketCount(){
        return marketCount;
    }
     
    /**
    * Count of markets associated with this marketType
    */

    public void setMarketCount(Integer marketCount){
        this.marketCount = marketCount;
    }
    
}
