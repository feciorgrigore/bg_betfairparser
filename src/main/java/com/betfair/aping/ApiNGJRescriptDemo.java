package com.betfair.aping;

import com.betfair.aping.api.ApiNgOperations;
import com.betfair.aping.api.ApiNgRescriptOperations;
import com.betfair.aping.entities.*;
import com.betfair.aping.enums.*;
import com.betfair.aping.exceptions.APINGException;

import java.util.*;

/**
 * This is a demonstration class to show a quick demo of the new Betfair API-NG.
 * When you execute the class will: <li>find a market (next horse race in the
 * UK)</li> <li>get prices and runners on this market</li> <li>place a bet on 1
 * runner</li> <li>handle the error</li>
 *
 *
 * https://developer.betfair.com/assets/BetfairSportsExchangeAPIReferenceGuidev6.pdf
 * 
 */
public class ApiNGJRescriptDemo {

	private ApiNgOperations rescriptOperations = ApiNgRescriptOperations.getInstance();
	private String applicationKey;
	private String sessionToken;


    public void getUnsettledBets(String appKey, String ssoid) throws APINGException {

        TimeRange dateRange = new TimeRange();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -10);
        dateRange.setFrom(calendar.getTime());
        dateRange.setTo(new Date());

        CurrentOrderSummaryReport currentOrderSummaryReport =
             rescriptOperations.getUnmatchedBets(new HashSet<String>(), new HashSet<String>(), dateRange, OrderProjection.EXECUTABLE,
        50, appKey, ssoid);

        System.out.println("Number of results: " + currentOrderSummaryReport.getCurrentOrders().size());

    }



    public void getTomorrowsDesiredMarkets(String appKey, String ssoid) throws APINGException {
        this.applicationKey = appKey;
        this.sessionToken = ssoid;
        try {

            MarketFilter marketFilter;
            marketFilter = new MarketFilter();
            Set<String> eventTypeIds = new HashSet<String>();
            eventTypeIds.add("1");

            TimeRange time = new TimeRange();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            time.setFrom(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            time.setTo(calendar.getTime());


            Set<String> marketTypesCode = new HashSet<String>();
            marketTypesCode.add("MATCH_ODDS");


            marketFilter = new MarketFilter();
            marketFilter.setEventTypeIds(eventTypeIds);
            marketFilter.setMarketStartTime(time);
            marketFilter.setMarketTypeCodes(marketTypesCode);


            Set<MarketProjection> marketProjection = new HashSet<MarketProjection>();
            marketProjection.add(MarketProjection.EVENT);


            String maxResults = "50";

            List<MarketCatalogue> marketCatalogueResult = rescriptOperations.listMarketCatalogue(marketFilter, marketProjection, MarketSort.FIRST_TO_START, maxResults,
                    applicationKey, sessionToken);

            System.out.println("5. Print static marketId, name and runners....\n");
            printMarketCatalogue(marketCatalogueResult.get(0));

            PriceProjection priceProjection = new PriceProjection();
            Set<PriceData> priceData = new HashSet<PriceData>();
            priceData.add(PriceData.EX_BEST_OFFERS);
            priceProjection.setPriceData(priceData);

            List<String> marketIds = new ArrayList<String>();

            for (MarketCatalogue marketCatalogue : marketCatalogueResult) {
                marketIds.add(marketCatalogue.getMarketId());
            }

//            List<MarketBook> marketBookReturn = rescriptOperations.listMarketBook(marketIds.subList(0, 10), priceProjection,
//                    null, null, null, applicationKey, sessionToken);

            maxResults = "50";
        } catch (APINGException apiExc) {
            System.out.println(apiExc.toString());
        }
    }





    public void start(String appKey, String ssoid) {

        this.applicationKey = appKey;
        this.sessionToken = ssoid;

        try {

            MarketFilter marketFilter;
            marketFilter = new MarketFilter();
            Set<String> eventTypeIds = new HashSet<String>();
            eventTypeIds.add("1");

            /**
             * ListMarketCatalogue: Get next available horse races, parameters:
             * eventTypeIds : 7 - get all available horse races for event id 7 (horse racing)
             * maxResults: 1 - specify number of results returned (narrowed to 1 to get first race)
             * marketStartTime: specify date (must be in this format: yyyy-mm-ddTHH:MM:SSZ)
             * sort: FIRST_TO_START - specify sort order to first to start race
             */
            TimeRange time = new TimeRange();
            time.setFrom(new Date());

//            Set<String> countries = new HashSet<String>();
//            countries.add("GB");

            Set<String> marketTypesCode = new HashSet<String>();
            marketTypesCode.add("MATCH_ODDS");


            marketFilter = new MarketFilter();
            marketFilter.setEventTypeIds(eventTypeIds);
            marketFilter.setMarketStartTime(time);
//            marketFilter.setMarketCountries(countries);
            marketFilter.setMarketTypeCodes(marketTypesCode);


            Set<MarketProjection> marketProjection = new HashSet<MarketProjection>();
            marketProjection.add(MarketProjection.EVENT);
//            marketProjection.add(MarketProjection.MARKET_DESCRIPTION);
//            marketProjection.add(MarketProjection.MARKET_START_TIME);
//            marketProjection.add(MarketProjection.RUNNER_DESCRIPTION);


            String maxResults = "50";

            List<MarketCatalogue> marketCatalogueResult = rescriptOperations.listMarketCatalogue(marketFilter, marketProjection, MarketSort.FIRST_TO_START, maxResults,
                    applicationKey, sessionToken);

            System.out.println("5. Print static marketId, name and runners....\n");
            printMarketCatalogue(marketCatalogueResult.get(0));
            /**
             * ListMarketBook: get list of runners in the market, parameters:
             * marketId:  the market we want to list runners
             *
             */
            System.out.println("6.(listMarketBook) Get volatile info for MarketStat including best 3 exchange prices available...\n");
            String marketIdChosen = marketCatalogueResult.get(0).getMarketId();

            PriceProjection priceProjection = new PriceProjection();
            Set<PriceData> priceData = new HashSet<PriceData>();
            priceData.add(PriceData.EX_BEST_OFFERS);
            priceProjection.setPriceData(priceData);

            //In this case we don't need these objects so they are declared null
            OrderProjection orderProjection = null;
            MatchProjection matchProjection = null;
            String currencyCode = null;

            List<String> marketIds = new ArrayList<String>();
            marketIds.add(marketIdChosen);

//            List<MarketBook> marketBookReturn = rescriptOperations.listMarketBook(marketIds, priceProjection,
//                    orderProjection, matchProjection, currencyCode, applicationKey, sessionToken);

            /**
             * PlaceOrders: we try to place a bet, based on the previous request we provide the following:
             * marketId: the market id
             * selectionId: the runner selection id we want to place the bet on
             * side: BACK - specify side, can be Back or Lay
             * orderType: LIMIT - specify order type
             * size: the size of the bet
             * price: the price of the bet
             * customerRef: 1 - unique reference for a transaction specified by user, must be different for each request
             *
             */

//            long selectionId = 0;
//            if ( marketBookReturn.size() != 0 ) {
//                Runner runner = marketBookReturn.get(0).getRunners().get(0);
//                selectionId = runner.getSelectionId();
//                System.out.println("7. Place a bet below minimum stake to prevent the bet actually " +
//                        "being placed for marketId: "+marketIdChosen+" with selectionId: "+selectionId+"...\n\n");
//                List<PlaceInstruction> instructions = new ArrayList<PlaceInstruction>();
//                PlaceInstruction instruction = new PlaceInstruction();
//                instruction.setHandicap(0);
//                instruction.setSide(Side.BACK);
//                instruction.setOrderType(OrderType.LIMIT);
//
//                LimitOrder limitOrder = new LimitOrder();
//                limitOrder.setPersistenceType(PersistenceType.LAPSE);
//                //API-NG will return an error with the default size=0.01. This is an expected behaviour.
//                //You can adjust the size and price value in the "apingdemo.properties" file
//                limitOrder.setPrice(getPrice());
//                limitOrder.setSize(getSize());
//
//                instruction.setLimitOrder(limitOrder);
//                instruction.setSelectionId(selectionId);
//                instructions.add(instruction);
//
//                String customerRef = "1";
//
//                PlaceExecutionReport placeBetResult = rescriptOperations.placeOrders(marketIdChosen, instructions, customerRef, applicationKey, sessionToken);
//
//                // Handling the operation result
//                if (placeBetResult.getStatus() == ExecutionReportStatus.SUCCESS) {
//                    System.out.println("Your bet has been placed!!");
//                    System.out.println(placeBetResult.getInstructionReports());
//                } else if (placeBetResult.getStatus() == ExecutionReportStatus.FAILURE) {
//                    System.out.println("Your bet has NOT been placed :*( ");
//                    System.out.println("The error is: " + placeBetResult.getErrorCode() + ": " + placeBetResult.getErrorCode().getMessage());
//                }
//            } else {
//                System.out.println("Sorry, no runners found\n\n");
//            }

        } catch (APINGException apiExc) {
            System.out.println(apiExc.toString());
        }
    }

    private static double getPrice() {

        try {
            return new Double((String) ApiNGDemo.getProp().get("BET_PRICE"));
        } catch (NumberFormatException e) {
            //returning the default value
            return new Double(1000);
        }

    }

    private static double getSize(){
        try{
            return new Double((String)ApiNGDemo.getProp().get("BET_SIZE"));
        } catch (NumberFormatException e){
            //returning the default value
            return new Double(0.01);
        }
    }

    private void printMarketCatalogue(MarketCatalogue mk){
        System.out.println("MarketStat Name: "+mk.getMarketName() + "; Id: "+mk.getMarketId()+"\n");
        List<RunnerCatalog> runners = mk.getRunners();
        if(runners!=null){
            for(RunnerCatalog rCat : runners){
                System.out.println("Runner Name: "+rCat.getRunnerName()+"; Selection Id: "+rCat.getSelectionId()+"\n");
            }
        }
    }

}
