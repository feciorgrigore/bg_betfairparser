package com.betfair.aping;

import com.betfair.aping.exceptions.APINGException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * This is a demonstration class to show a quick demo of the new Betfair API-NG.
 * When you execute the class will: <li>find a market (next horse race in the
 * UK)</li> <li>get prices and runners on this market</li> <li>place a bet on 1
 * runner</li> <li>handle the error</li>
 *
 */
public class ApiNGDemo {

    private static Properties prop = new Properties();
    private static Boolean jsonRpcRequest;
    private static String applicationKey;
    private static String sessionToken;
    private static String jsonOrRescript;
    private static boolean debug;

    static {
        try {
            InputStream in = ApiNGDemo.class.getResourceAsStream("/apingdemo.properties");
            prop.load(in);
            in.close();

            debug = new Boolean(prop.getProperty("DEBUG"));

        } catch (IOException e) {
            System.out.println("Error loading the properties file: "+e.toString());
        }
    }

    public static void main(String[] args) throws APINGException {

        System.out.println("Welcome to the Betfair API NG!");

        // obtained from https://developer.betfair.com/visualisers/api-ng-account-operations/
        applicationKey = "uSmjWwT0FCxRoXCI";
        sessionToken = "4q8PCjxJc0pY6xxppL27H9rq5cx57eXMni92M+Hspj8=";

        ApiNGJRescriptDemo rescriptDemo = new ApiNGJRescriptDemo();
//        rescriptDemo.start(applicationKey, sessionToken);

//        rescriptDemo.getUnsettledBets(applicationKey, sessionToken);


        rescriptDemo.getTomorrowsDesiredMarkets(applicationKey, sessionToken);
    }


    public static Properties getProp() {
        return prop;
    }

    public static boolean isDebug(){
        return debug;
    }
}
